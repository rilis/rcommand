'use strict';
var childProcess = require("child_process");

function Connection(socket) {
    this.socket = socket;
    this.command = undefined;

    this.stdinClosed = false;
    this.stdoutClosed = false;
    this.stderrClosed = false;
    this.commandClosed = false;
    this.commandExitStatus = -1;
    this.haveError = false;

}

function parseArgsStringToArgv(value) {
    var myRegexp = /([^\s'"]+(['"])([^\2]*?)\2)|[^\s'"]+|(['"])([^\4]*?)\4/gi;
    var myString = value;
    var myArray = [];
    var match;
    do {
        match = myRegexp.exec(myString);
        if (match !== null) {
            myArray.push(match[1] || match[5] || match[0]);
        }
    } while (match !== null);

    return myArray;
}

Connection.prototype.handleExecuteCommand = function(cmd) {
    var self = this;
    if (this.command === undefined) {
        var args = parseArgsStringToArgv(cmd);
        var child = childProcess.spawn(args[0], args.slice(1), {
            stdio: ["pipe", "pipe", "pipe"],
            shell: true
        });

        child.stdin.on("close", function() {
            self.stdinClosed = true;
        });
        child.stdin.on("error", function() {
            self.stdinClosed = true;
        });
        child.stdout.on("data", function(chunk) {
            self.sendMessage({
                id: "stdoutData",
                data: chunk.toString('hex')
            });
        });
        child.stdout.on("end", function() {
            self.stdoutClosed = true;
            self.sendMessage({
                id: "stdoutEnd"
            });
            self.tryComplete();
        });
        child.stderr.on("data", function(chunk) {
            self.sendMessage({
                id: "stderrData",
                data: chunk.toString('hex')
            });
        });
        child.stderr.on("end", function() {
            self.stderrClosed = true;
            self.sendMessage({
                id: "stderrEnd"
            });
            self.tryComplete();
        });

        child.on("error", function(err) {
            self.handleError();
        });
        child.on("exit", function(code, sig) {
            self.commandClosed = true;
            self.commandExitStatus = code;
            self.tryComplete();
        });

        this.command = child;
    } else {
        this.handleError();
    }
};

Connection.prototype.handleStdinData = function(data) {
    if (this.command !== undefined) {
        if (this.stdinClosed === false) {
            this.command.stdin.write(data);
        }
    } else {
        this.handleError();
    }
};

Connection.prototype.handleStdinEnd = function() {
    if (this.command !== undefined) {
        if (this.stdinClosed === false) {
            this.command.stdin.end();
        }
    } else {
        this.handleError();
    }
};

Connection.prototype.handleClientMessage = function(message) {
    if (message.id === "executeCommand") {
        this.handleExecuteCommand(message.command);
    } else if (message.id === "stdinData") {
        this.handleStdinData(Buffer.from(message.data, "hex"));
    } else if (message.id === "stdinEnd") {
        this.handleStdinEnd();
    } else {
        this.handleError();
    }
};

Connection.prototype.tryComplete = function() {

    if (this.stdoutClosed === true &&
        this.stderrClosed === true &&
        this.commandClosed === true) {
        this.sendMessage({
            id: "commandEnd",
            status: this.commandExitStatus
        });
    }
};

Connection.prototype.handleError = function() {
    this.haveError = true;
    if (this.command) {
        this.command.kill();
    }
    if (this.socket) {
        this.socket.terminate();
    }
};

Connection.prototype.onClose = function(code, reason) {
    this.handleError();
};

Connection.prototype.onError = function(error) {
    this.handleError();
};

Connection.prototype.onMessage = function(message) {
    try {
        this.handleClientMessage(JSON.parse(message));
    } catch (e) {
        console.error("rcommand: invalid message from client!");
        console.error(e);
        this.handleError();
    }
};

Connection.prototype.sendMessage = function(message) {
    if (this.haveError === false) {
        var payload = JSON.stringify(message);
        this.socket.send(payload);
    }
};

function handleConnection(socket) {
    var connection = new Connection(socket);
    socket.on("close", function(code, reason) {
        connection.onClose(code, reason);
    });
    socket.on("error", function(error) {
        connection.onError(error);
    });
    socket.on("message", function(message) {
        connection.onMessage(message);
    });
}

module.exports = handleConnection;