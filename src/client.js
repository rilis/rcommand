'use strict';

function Connection(socket, command) {
    this.socket = socket;
    this.command = command;
    this.active = false;
    this.stdoutCompleted = false;
    this.stderrCompleted = false;
    this.commandCompleted = false;
    this.commandExitCode = -1;
}

Connection.prototype.tryComplete = function() {
    if (this.stdoutCompleted === true && this.stderrCompleted === true && this.commandCompleted === true) {
        process.exit(this.commandExitCode);
    }
};

Connection.prototype.sendMessage = function(message) {
    if (this.active === true) {
        var payload = JSON.stringify(message);
        this.socket.send(payload);
    }
};

Connection.prototype.onClose = function(code, reason) {
    console.error("rcommand: unexpected connection close!");
    process.exit(-6);
};

Connection.prototype.onError = function(error) {
    console.error("rcommand: unexpected connection error!");
    process.exit(-7);
};

Connection.prototype.onMessage = function(message) {
    try {
        this.handleServerMessage(JSON.parse(message));
    } catch (e) {
        console.error("rcommand: invalid message from server: %j!", e);
        process.exit(-8);
    }
};

Connection.prototype.handleServerMessage = function(message) {
    var self = this;
    if (message.id === "stdoutData") {
        process.stdout.write(Buffer.from(message.data, "hex"));
    } else if (message.id === "stdoutEnd") {
        self.stdoutCompleted = true;
        self.tryComplete();
    } else if (message.id === "stderrData") {
        process.stderr.write(Buffer.from(message.data, "hex"));
    } else if (message.id === "stderrEnd") {
        self.stderrCompleted = true;
        self.tryComplete();
    } else if (message.id === "commandEnd") {
        self.commandCompleted = true;
        self.commandExitCode = message.status;
        self.tryComplete();
    } else {
        console.error("rcommand: unknown message type");
        process.exit(-9);
    }
};

Connection.prototype.onOpen = function() {
    this.active = true;
    this.sendMessage({
        id: "executeCommand",
        command: this.command
    });

    var self = this;
    process.stdin.on("data", function(chunk) {
        self.onStdinData(chunk);
    });
    process.stdin.on("end", function() {
        self.onStdinEnd();
    });
};

Connection.prototype.onStdinData = function(chunk) {
    this.sendMessage({
        id: "stdinData",
        data: chunk.toString('hex')
    });
};

Connection.prototype.onStdinEnd = function() {
    this.sendMessage({
        id: "stdinEnd"
    });
};

function handleConnection(socket, command) {
    var connection = new Connection(socket, command);
    socket.on("close", function(code, reason) {
        connection.onClose(code, reason);
    });
    socket.on("error", function(error) {
        connection.onError(error);
    });
    socket.on("message", function(message) {
        connection.onMessage(message);
    });
    socket.on("open", function() {
        connection.onOpen();
    });
}

module.exports = handleConnection;