'use strict';
var WebSocket = require('ws');

function client(ip, port, command) {
    try {
        require("./client.js")(new WebSocket("ws://" + ip + ":" + port + "/"), command);
    } catch (e) {
        console.error("rcommand: unexpected client connection creation error %j", e);
        process.exit(-2);
    }
}

function server(ip, port) {
    try {
        var wss = new WebSocket.Server({
            host: ip,
            port: port
        });

        wss.on("close", function() {
            console.error("rcommand: Unexpected server close!");
            process.exit(-3);
        });
        wss.on("error", function() {
            console.error("rcommand: Unexpected server error!");
            process.exit(-4);
        });
        wss.on("listening", function() {
            console.log("rcommand: Server up and running!");
        });
        wss.on("connection", require("./server.js"));
    } catch (e) {
        console.error("rcommand: Unexpected server error %j!", e);
        process.exit(-5);
    }
}

function showHelp() {
    console.error("rcommand: v0.0.0");
    console.error("Use: rcommand IP PORT <\"COMMAND_TO_EXECUTE\">\n");
    console.error("   * IP - ip which should be used by rcommand");
    console.error("   * PORT - port which should be used by rcommand");
    console.error("   * COMMAND_TO_EXECUTE - this parameter is used only in client mode and used to specify whole command which should be executed on server");

    console.error("\nnote: this software is TOTTALLY INSECURE please DO NOT USE IT when security matters");
}

function main(args, cwd) {
    if (args.length === 2) {
        server(args[0], args[1]);
    } else if (args.length === 3) {
        client(args[0], args[1], args[2]);
    } else {
        showHelp();
        process.exit(-1);
    }
}

module.exports = main;