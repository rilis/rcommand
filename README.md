# rcommand

rcommand is very simple script, which allow execute remotely commands. It's totally insecure, so do not use it outside your local pc.

It was created to run on VM with windows commands from host machine and upload files in CI jobs, when SSH is not avaliable on windows and/or bugprone.

rcommand works in server and client mode on selected by you port and host.

### Installation

```
npm install -g rili-rcommand
```


### example server incantation:

```
$ rcommand 0.0.0.0 44444
```

### example client incantation:

```
$ rcommand 1.2.3.4 44444 "cat > someFile" < someFile # will copy someFile to server from client machine
```

```
$ rcommand 1.2.3.4 44444 "cat < someOtherFile" > someOtherFile # will copy someOtherFile from server to client machine
```


## [Issue Tracker](https://gitlab.com/rilis/rilis/issues)
